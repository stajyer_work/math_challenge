package com.medipic.mathfreak.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.LeaderboardsClient;
import com.medipic.mathfreak.R;
import com.medipic.mathfreak.Utils.Leaderboards;

public class Level {

    private final float levelUpCoff = 1.4f;
    private final float PI = 3.14f;
    private final int startTotalXP= 1000;
    private final int startLevel=1;
    private final int startCurrentXp =0;

    private SharedPreferences preferences;

    private final String prefsLevel = "LEVEL";
    private final String prefsCurrentXP = "CURRENT_XP";
    private final String prefsTotalXP = "TOTAL_XP";

    public Context context;

    private LeaderboardsClient mLeaderboardsClient;

    public Level(Context context){
        this.context = context;
        preferences = context.getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
    }

    public int getLevelTotalXP(){
        int temp;
        try{
            temp = preferences.getInt(prefsTotalXP,0);
            if(temp==0){
                temp=startTotalXP;
            }
        }catch (NullPointerException e){
            temp = startTotalXP;
        }
        return temp;
    }


    public int getLevelCurrentXP(){
        int temp;
        try{
            temp = preferences.getInt(prefsCurrentXP,0);
            if(temp==0){
                temp=startCurrentXp;
            }
        }catch (NullPointerException e){
            temp = startCurrentXp;
        }
        return temp;
    }

    public int getLevel(){
        int temp;
        try{
            temp = preferences.getInt(prefsLevel,0);
            if(temp==0){
                temp = startLevel;
            }
        }catch (NullPointerException e){
            temp = startLevel;
        }
        return temp;
    }

    public void levelUP(){
        preferences.edit().putInt(prefsLevel,getLevel()+1).apply();
        preferences.edit().putInt(prefsTotalXP,Math.round(getLevelTotalXP()*levelUpCoff)).apply();
    }


    public void saveCurrentXP(int currentXp){
        preferences.edit().putInt(prefsCurrentXP,currentXp).apply();
    }

    public long calculateXPfromScore(int score,long time,int diff){
        Leaderboards leaderboards = new Leaderboards();
        double seconds = time/10000.0d;
        long score_;
        switch (diff){
            case 1:
                score_ = Math.round(score*(1.0d/seconds)*3.14f);
                if(score_ > preferences.getInt(context.getResources().getString(R.string.easy_high_score_tag),0)){
                    new EasyTask(leaderboards,Math.round(score_),context).execute();
                }

                return score_;
            case 2:
                score_ = Math.round(score*(1.0d/seconds)*5.14f);
                if(score_ > preferences.getInt(context.getResources().getString(R.string.medium_high_score_tag),0)){
                    new MediumTask(leaderboards,Math.round(score_),context).execute();
                }

                return score_;
            case 3:
                score_ = Math.round(score*(1.0d/seconds)*7.14f);
                if(score_ > preferences.getInt(context.getResources().getString(R.string.hard_high_score_tag),0) ){
                    new HardTask(leaderboards,Math.round(score_),context).execute();
                }

                return score_;
        }

        return 0;
    }


    private class EasyTask extends AsyncTask<Void,Void,Void>{
        int score_;
        Leaderboards leaderboards;
        Context context;
        public EasyTask(Leaderboards leaderboards,int score,Context context){
            this.leaderboards = leaderboards;
            this.score_=score;
            this.context=context;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try{
                preferences.edit().putInt(context.getResources().getString(R.string.easy_high_score_tag),score_).apply();
                try{
                    mLeaderboardsClient = Games.getLeaderboardsClient(context, GoogleSignIn.getLastSignedInAccount(context));
                    mLeaderboardsClient.submitScore(context.getResources().getString(leaderboards.getEasy_ids()[0]),Math.round(score_));
                    mLeaderboardsClient.submitScore(context.getResources().getString(leaderboards.getEasy_ids()[getLevel()]),Math.round(score_));
                }catch (Exception e){

                }


            }catch (Exception e){
                Toast.makeText(context,"Google Play Hesabı Bağlı Değil",Toast.LENGTH_LONG).show();
            }
            return null;
        }
    }

    private class MediumTask extends AsyncTask<Void,Void,Void>{
        int score_;
        Leaderboards leaderboards;
        Context context;
        public MediumTask(Leaderboards leaderboards,int score,Context context){
            this.leaderboards = leaderboards;
            this.score_=score;
            this.context=context;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try{
                preferences.edit().putInt(context.getResources().getString(R.string.medium_high_score_tag),score_).apply();
                mLeaderboardsClient = Games.getLeaderboardsClient(context, GoogleSignIn.getLastSignedInAccount(context));
                mLeaderboardsClient.submitScore(context.getResources().getString(leaderboards.getMedium_ids()[0]),Math.round(score_));
                mLeaderboardsClient.submitScore(context.getResources().getString(leaderboards.getMedium_ids()[getLevel()]),Math.round(score_));

            }catch (Exception e){
                Toast.makeText(context,"Google Play Hesabı Bağlı Değil",Toast.LENGTH_LONG).show();
            }
            return null;
        }
    }
    private class HardTask extends AsyncTask<Void,Void,Void>{
        int score_;
        Leaderboards leaderboards;
        Context context;
        public HardTask(Leaderboards leaderboards,int score,Context context){
            this.leaderboards = leaderboards;
            this.score_=score;
            this.context=context;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try{
                preferences.edit().putInt(context.getResources().getString(R.string.hard_high_score_tag),score_).apply();
                mLeaderboardsClient = Games.getLeaderboardsClient(context, GoogleSignIn.getLastSignedInAccount(context));
                mLeaderboardsClient.submitScore(context.getResources().getString(leaderboards.getHard_ids()[0]),Math.round(score_));
                mLeaderboardsClient.submitScore(context.getResources().getString(leaderboards.getHard_ids()[getLevel()]),Math.round(score_));
            }catch (Exception e){
                Toast.makeText(context,"Google Play Hesabı Bağlı Değil",Toast.LENGTH_LONG).show();
            }

            return null;
        }
    }


}
