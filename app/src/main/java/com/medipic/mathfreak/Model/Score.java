package com.medipic.mathfreak.Model;

import android.util.Log;

public class Score {

    private final int easy = 30;
    private final int medium=45;
    private final int hard = 60;

    private int correctN= 0;
    private int falseN = 0;
    private int score=0;

    private int diff;

    public Score (int diff){
        this.diff = diff;
        Log.e("Score.java",diff+"");
    }

    private void setScore(){
        this.score+=correctN*getCoffDiff(diff);
    }

    public int getScore(){
        setScore();
        return score; }

    public void incCorrectN(){
        this.correctN++;
    }

    public int getCorrectN(){
        return this.correctN;
    }

    public void incFalseN(){
        this.falseN++;
    }

    public int getFalseN(){
        return this.falseN;
    }

    private void saveScore(int score){}

    public int getCoffDiff(int n){
        switch (n){
            case 1:
                return easy;
            case 2:
                return medium;
            case 3:
                return hard;
        }
        Log.e("Score.java","line 58");
        return 0;
    }
}
