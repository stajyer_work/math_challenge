package com.medipic.mathfreak.Model;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;

import com.medipic.mathfreak.Activities.GameScreen;
import com.medipic.mathfreak.R;

public class Timer {

    private View view;
    private GameScreen gameScreen;
    private int progressCount =0;
    private int total=0;
    private int incrementValue=0;

    public static CountDownTimer countDownTimer;

    public Timer(GameScreen activity, View view , int total, int incrementValue){
        this.gameScreen = activity;
        this.view=view;
        this.total=total;
        this.incrementValue = incrementValue;
    }


    public CountDownTimer getCountDownTimer() {
        return this.countDownTimer;
    }

    public void setView(){
        countDownTimer = new CountDownTimer(total,incrementValue){

            @Override
            public void onTick(long l) {
                setProgressBar();
            }

            @Override
            public void onFinish() {

                try{
                    if(gameScreen.getQuestion().getCorrectAnswer() == gameScreen.getAnswer()){
                        gameScreen.answerCorrect();
                    }
                    else
                        gameScreen.answerFalse();
                }catch(Exception e){
                    e.printStackTrace();
                    gameScreen.answerFalse();
                }

                countDownTimer.cancel();

            }
        }.start();
    }

    private void setProgressBar(){
        progressCount+=1;
        ProgressBar progressBar = view.findViewById(R.id.question_timer_progress);
        progressBar.setProgress(progressCount);
    }





    private boolean progressFinished(){
       if(progressCount==total)
           return true;

       return false;
    }
}
