package com.medipic.mathfreak.Model;

import android.util.Log;
import android.view.View;

import com.medipic.mathfreak.Utils.EvaluateString;

import java.util.ArrayList;

public class Question {

    public static int QUESNUM = 10;

    private String question;
    private StringBuilder strBuilder;
    private ArrayList<String> strList;
    private String operators [] = {"+","-","*","/"};

    private final int easyOperatorNum = 1, easyMin = 1, easyMax = 12;

    private final int mediumOperaterNum = 2, mediumMin = 1, mediumMax = 10;

    private final int hardOperaterCount = 3, hardMin = 1, hardMax = 10;

    private int quesNum=0;
    private View view;

    public Question(){
        strBuilder = new StringBuilder();
    }


    public String getQuestion(int diff){
        quesNum++;
        switch (diff){
            case 1:
                return getEasyQues();
            case 2:
                return getMediumQues();
            case 3:
                return getHardQues();
        }
        return null;
    }

    public int getQuesNum(){
        return quesNum;
    }

    public int getCorrectAnswer(){
        return evaluateString(question);
    }
    private String getEasyQues(){
        return createQuestion(easyOperatorNum,easyMin,easyMax);
    }

    private String getMediumQues(){return createQuestion(mediumOperaterNum,mediumMin,mediumMax);}

    private String getHardQues(){return createQuestion(hardOperaterCount,hardMin,hardMax);}

    private String getRandomInt(int min,int max){
        return String.valueOf((int)((Math.random()*(max+1-min)) + min));
    }

    private String getOperator(){
        return operators[(int)(Math.random()*4)];
    }

    public String createQuestion(int operaterCount,int min ,int max){
        strList = new ArrayList<>();
        for(int i=1;i<=((operaterCount*2)+1);i++){
            if(i%2 ==0){
                strList.add(getOperator()+" ");
            }
            else{
                if(i!=1 && strList.get(i-2).equalsIgnoreCase("/ ")){
                        strList.add(String.valueOf(getPrimeFactor(evaluateString(createStringExpression(i-2))))+" ");
                }
                else{
                    strList.add(getRandomInt(min,max)+" ");
                }
            }
        }

        question = createStringExpression(strList.size());

        Log.e(question+"","");

        return question;
    }

    public int evaluateString(String str){
        EvaluateString eva = new EvaluateString();
        return eva.evaluate(str);
    }

    public int getPrimeFactor(int num){
        try{
            if(num != 0 && num !=1){
                ArrayList<Integer> arrList = new ArrayList<>();
                for(int i=2;i<=num;i++){
                    while (num % i == 0) {
                        arrList.add(i);
                        num /= i;
                    }
                }

                return arrList.get((int)(Math.random()*arrList.size()));
            }
            else
                return num==0?1:num;
        }catch(Exception e){
            return 1;
        }

    }

    public String createStringExpression(int lastIndex){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0;i<lastIndex;i++){
            stringBuilder.append(strList.get(i));
        }

        return stringBuilder.toString();
    }


}
