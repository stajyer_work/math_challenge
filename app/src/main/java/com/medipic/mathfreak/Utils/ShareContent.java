package com.medipic.mathfreak.Utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.StrictMode;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;

public class ShareContent {

    Context context;
    public ShareContent(Context context){

        this.context=context;

    }
    public Bitmap getBitmapOfView(View view){

        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(),600,Bitmap.Config.ARGB_8888);
        //Drawable drawable = context.getResources().getDrawable(R.drawable.app_signature);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable!=null) {
            bgDrawable.draw(canvas);
        }   else{
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        /*drawable.setBounds(0,view.getHeight(),view.getWidth(),view.getHeight()+100);
        drawable.draw(canvas);*/
        return returnedBitmap;

    }

    public void share(View view){
        Bitmap bitmap = getBitmapOfView(view);
        try {
            File file = new File(context.getCacheDir(),"shareprofil.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);

            //Değiştirilecek
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            //Değiştirilecek

            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            context.startActivity(Intent.createChooser(intent, "Şununla Paylaş :"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
