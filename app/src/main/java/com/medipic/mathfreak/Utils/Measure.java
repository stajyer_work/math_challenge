package com.medipic.mathfreak.Utils;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Measure {

    Display display;
    public static Context context;
    public static float width,height;
    View view;
    public Measure(Context context){
        this.context = context;

        setDimensPixels();

    }

    public Measure (){}

    public float convertDpToPx(float dp) {
        try {
            float width = 15 * context.getResources().getDisplayMetrics().density;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return width ;
    }

    public float convertPxToDp(float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public void setDimensPixels(){
        try{
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            display = wm.getDefaultDisplay();
        }catch(NullPointerException e){
            Toast.makeText(context,"NullPointerException",Toast.LENGTH_LONG).show();
        }
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    public float getWidth(){
        return width;
    }

    public float getHeight(){
        return height;
    }

    public float getWidth(View view){
        view.measure(0,0);
        return view.getMeasuredWidth();
    }

    public float getHeight(View view){
        view.measure(0,0);
        return view.getMeasuredHeight();
    }

    public RelativeLayout.LayoutParams getLayoutParams(View view){
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
        return lp;
    }





}
