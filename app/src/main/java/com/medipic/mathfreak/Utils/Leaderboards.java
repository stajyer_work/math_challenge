package com.medipic.mathfreak.Utils;

import com.medipic.mathfreak.R;

public class Leaderboards {

    public Leaderboards(){}
    public int easy_ids []= {R.string.leaderboard_easy_general,
            R.string.leaderboard_easy_level_1,
            R.string.leaderboard_easy_level_2,
            R.string.leaderboard_easy_level_3,
            R.string.leaderboard_easy_level_4,
            R.string.leaderboard_easy_level_5,
            R.string.leaderboard_easy_level_6,
            R.string.leaderboard_easy_level_7,
            R.string.leaderboard_easy_level_8,
            R.string.leaderboard_easy_level_9,
            R.string.leaderboard_easy_level_10,

    };

    public int medium_ids [] = {R.string.leaderboard_medium_general,
            R.string.leaderboard_medium_level_1,
            R.string.leaderboard_medium_level_2,
            R.string.leaderboard_medium_level_3,
            R.string.leaderboard_medium_level_4,
            R.string.leaderboard_medium_level_5,
            R.string.leaderboard_medium_level_6,
            R.string.leaderboard_medium_level_7,
            R.string.leaderboard_medium_level_8,
            R.string.leaderboard_medium_level_9,
            R.string.leaderboard_medium_level_10,
    };

    public int hard_ids [] = {R.string.leaderboard_hard_general,
            R.string.leaderboard_hard_level_1,
            R.string.leaderboard_hard_level_2,
            R.string.leaderboard_hard_level_3,
            R.string.leaderboard_hard_level_4,
            R.string.leaderboard_hard_level_5,
            R.string.leaderboard_hard_level_6,
            R.string.leaderboard_hard_level_7,
            R.string.leaderboard_hard_level_8,
            R.string.leaderboard_hard_level_9,
            R.string.leaderboard_hard_level_10,
    };

    public int[] getEasy_ids() {
        return easy_ids;
    }

    public int[] getHard_ids() {
        return hard_ids;
    }

    public int[] getMedium_ids() {
        return medium_ids;
    }
}
