package com.medipic.mathfreak.Utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.os.Handler;
import android.view.animation.Animation;

import com.airbnb.lottie.LottieAnimationView;

import com.medipic.mathfreak.Activities.GameScreen;
import com.medipic.mathfreak.Model.Question;
import com.medipic.mathfreak.R;

public class LottieAnimations implements Animation.AnimationListener{

    private final long correctAnimationDurarionTime = 1500;
    private final long falseAnimationDurarionTime = 1500;

    private Runnable runnable;
    private Handler handler;
    private LottieAnimationView correctAnimation,falseAnimation;
    private int translateX,translateY;
    Measure measure;
    Question question;
    Context context;
    GameScreen gameScreen;
    ProgressBarPoints progressBarPoints;
    Animations animations;
    Animation anim;
    public LottieAnimations(Context context,View view){
        this.context = context;
        handler = new Handler();

        correctAnimation= view.findViewById(R.id.correct_animation);
        correctAnimation.setAnimation("correct_animation_lottie.json");
        falseAnimation =view.findViewById(R.id.false_animation);
        falseAnimation.setAnimation("false_animation_lottie.json");

        setTranslationOfViews();

        gameScreen = (GameScreen) context;
        question = gameScreen.getQuestion();
        progressBarPoints = gameScreen.getProgressBarPoints();
        animations = new Animations(this.context);
        measure = new Measure();

    }


    public void playCorrectAnim(){
        try {
            correctAnimation.playAnimation();
            correctAnimation.setVisibility(View.VISIBLE);
            Log.e("Correct Animation","Start !");

            stopAnimation(1,correctAnimationDurarionTime);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void playFalseAnim(){
        try {
            falseAnimation.playAnimation();
            falseAnimation.setVisibility(View.VISIBLE);
            Log.e("False Animation","Start !");

            stopAnimation(2,falseAnimationDurarionTime);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setTranslationOfViews(){
        ViewCompat.setTranslationZ(correctAnimation, 25f);
        ViewCompat.setTranslationZ(falseAnimation, 25f);
    }

    public void stopAnimation(int choice,long durationTime){
        handler.postDelayed(getRunnable(choice),durationTime);
    }

    public Runnable getRunnable(int i){
        switch (i){
            case 1:
                runnable = new Runnable() {
                    @Override
                    public void run() {

                        int  [] cor = new int[2];
                        final float posX,posY;
                        correctAnimation.getLocationOnScreen(cor);
                        posX = progressBarPoints.getProgressBarPointsArrList(question.getQuesNum()-1)+correctAnimation.getWidth()/2;
                        posY = progressBarPoints.getProgressBarTopMargin()-cor[1];

                        anim = animations.getAnimationSet(posX,posY);

                        correctAnimation.startAnimation(anim);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                correctAnimation.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                correctAnimation.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        //correctAnimation.setVisibility(View.INVISIBLE);
                    }
                };

                return runnable;
            case 2:
                runnable = new Runnable() {
                    @Override
                    public void run() {

                        int  [] cor = new int[2];
                        final float posX,posY;
                        falseAnimation.getLocationOnScreen(cor);
                        posX = progressBarPoints.getProgressBarPointsArrList(question.getQuesNum()-1)+falseAnimation.getWidth()/2;
                        posY = progressBarPoints.getProgressBarTopMargin()-cor[1];

                        anim = animations.getAnimationSet(posX,posY);

                        falseAnimation.startAnimation(anim);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                falseAnimation.setVisibility(View.INVISIBLE);

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }
                };

                return runnable;
        }

        return null;
    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    private class AnimationSetTesk extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}
