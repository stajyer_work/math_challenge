package com.medipic.mathfreak.Utils;

import android.content.Context;
import android.view.View;

import com.medipic.mathfreak.Model.Question;

import java.util.ArrayList;

public class ProgressBarPoints {

    ArrayList<Float> progressBarPoints ;
    float progressBarMargin;
    Measure measure;
    Context context;
    float progressBarTopMargin;
    float getProgressStartMargin;
    View rootView;
    public ProgressBarPoints(Context context,View rootView){
        this.context = context;
        this.rootView = rootView;
        progressBarPoints = new ArrayList<>();
        measure = new Measure();
        setProgressBarPoints();
    }

    public void setProgressBarPoints(){
        progressBarMargin = getProgressStartMargin();
        float widthProgress = measure.getWidth()-(progressBarMargin*2);
        float quesInterval = widthProgress/ Question.QUESNUM;

        for(int i =0;i<Question.QUESNUM;i++){
            if(i<(Question.QUESNUM/2)){
                progressBarPoints.add(measure.convertDpToPx(getProgressStartMargin())
                        +(quesInterval*(-Question.QUESNUM/2+i)));
            }else{
                progressBarPoints.add(measure.convertDpToPx(getProgressStartMargin())
                        +(quesInterval*((i-Question.QUESNUM/2)+1)));
            }
        }
    }


    public float getProgressStartMargin() {
        return measure.convertDpToPx(measure.getLayoutParams(rootView).leftMargin);
    }

    public float getProgressBarTopMargin() {
        int [] cor = new int[2];
        rootView.getLocationOnScreen(cor);
        return cor[1];
    }

    public float getProgressBarPointsArrList(int i) {
        return progressBarPoints.get(i-1);
    }


}
