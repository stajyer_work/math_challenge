package com.medipic.mathfreak.Utils;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;

import com.medipic.mathfreak.R;

public class Animations {

    private final long GAME_SCREEN_ANIMATIONS_DURATION_TIME = 500;
    AnimationSet animationSet;
    Context context;
    public Animations(Context context){
        this.context = context;
    }

    public AnimationSet getAnimationSet(float toXDelta,float toYDelta){
        animationSet = new AnimationSet(true);
        Animation smallerAnim = AnimationUtils.loadAnimation(context, R.anim.scale_smaller_animation);
        Animation animTranslate = new TranslateAnimation(0,toXDelta,0,toYDelta);
        animationSet.addAnimation(smallerAnim);
        animationSet.addAnimation(animTranslate);
        animationSet.setDuration(GAME_SCREEN_ANIMATIONS_DURATION_TIME);

        return animationSet;
    }
}
