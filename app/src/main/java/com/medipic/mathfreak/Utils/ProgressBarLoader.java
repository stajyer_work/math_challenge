package com.medipic.mathfreak.Utils;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.medipic.mathfreak.Model.Level;
import com.medipic.mathfreak.R;

public class ProgressBarLoader {

    private CountDownTimer countDownTimer;
    private Context context;
    private ProgressBar progressBar;
    private TextView levelText;
    private float earnedXP;
    private int levelTotalXP,levelCurrentXP;
    private int counter;

    public ProgressBarLoader(Context context,ProgressBar progressBar,TextView tv,float earnedXP,int levelCurrentXP,int levelTotalXP){
        this.context = context;
        this.progressBar = progressBar;
        this.earnedXP = earnedXP;
        this.levelText = tv;
        this.levelCurrentXP = levelCurrentXP;
        this.levelTotalXP = levelTotalXP;
        setProgressBar();
    }


    private void setProgressBar(){
        countDownTimer = new CountDownTimer(15000,5) {
            @Override
            public void onTick(long l) {
                if(progressBar.getProgress()==levelTotalXP){
                    setProgressBarToLevelUp();
                }
                else if(counter==earnedXP)
                    countDownTimer.cancel();
                else{
                    counter++;
                    progressBar.setProgress(getCurrentProgress()+1);
                }

            }

            @Override
            public void onFinish() {

                Log.e("ProgressBarLoader (if) ",progressBar.getProgress()+"");
            }
        }.start();
    }

    private int getCurrentProgress(){
        return progressBar.getProgress();
    }


    private void setProgressBarToLevelUp(){
        Level level = new Level(context);
        progressBar.setMax(level.getLevelTotalXP());
        levelText.setText(String.valueOf(context.getResources().getString(R.string.level)+level.getLevel()));
        progressBar.setProgress(0);
    }


}
