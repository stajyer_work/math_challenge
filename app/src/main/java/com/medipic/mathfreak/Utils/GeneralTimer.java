package com.medipic.mathfreak.Utils;

public class GeneralTimer {

    private long time=0;

    public GeneralTimer(){}

    public void startTimer(){
        time = System.currentTimeMillis();
    }

    public long getTime(){
        return time ;
    }

    public void stopTimer(){
        time = System.currentTimeMillis()-time;
    }


}
