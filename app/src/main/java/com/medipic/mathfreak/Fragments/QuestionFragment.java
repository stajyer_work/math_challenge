package com.medipic.mathfreak.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.medipic.mathfreak.Activities.GameScreen;
import com.medipic.mathfreak.Activities.MainScreen;
import com.medipic.mathfreak.Model.Question;
import com.medipic.mathfreak.Model.Timer;
import com.medipic.mathfreak.R;
import com.medipic.mathfreak.Utils.GeneralTimer;

public class QuestionFragment extends Fragment {

    private final long easyQuesTime = 4000, mediumQuesTime = 6000, hardQuesTime = 8000;
    private final long easyQuesIntervalTime = 30, mediumQuesIntervalTime = 45, hardQuesIntervalTime = 70;
    //private final long countDownTimerfInterval = 1000;

    GeneralTimer generalTimer;
    Timer timer;
    View view;
    GameScreen gameScreen;
    TextView question_text;
    ProgressBar question_time_bar;
    Question question;
    public static String questionStr;
    //private int progressCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.question_layout, container, false);
        gameScreen = (GameScreen) getActivity();
        question = gameScreen.getQuestion();
        generalTimer = gameScreen.getGeneralTimer();
        timer = new Timer(gameScreen,view, (int) getDiffQuesTime(MainScreen.Diff),(int) getDiffIntervalTime(MainScreen.Diff));
        initializeViews(view);
        return view;
    }


    public void initializeViews(View view){
        question_text = view.findViewById(R.id.question_text);
        question_time_bar = view.findViewById(R.id.question_timer_progress);
        question_time_bar.setProgress(0);
        question_time_bar.setMax(100);
        setQuestion();
        setProgress();
    }

    public void setProgress(){
        timer.setView();
    }

    public void setQuestion(){
        questionStr = gameScreen.getQuestionStr();
        if(question.getQuesNum()==1){
            generalTimer.startTimer();
        }
        question_text.setText(String.valueOf(questionStr+" "+getResources().getString(R.string.equal_operater)+" "));
    }

    private long getDiffQuesTime(int diff){
        switch (diff){
            case 1:
                return easyQuesTime;
            case 2:
                return mediumQuesTime;
            case 3:
                return hardQuesTime;
        }

        return 0;
    }

    private long getDiffIntervalTime(int diff){
        switch (diff){
            case 1:
                return easyQuesIntervalTime;
            case 2:
                return mediumQuesIntervalTime;
            case 3:
                return hardQuesIntervalTime;
        }

        return 0;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("CountDownTimer","destroy !");
        timer.getCountDownTimer().cancel();
    }


}
