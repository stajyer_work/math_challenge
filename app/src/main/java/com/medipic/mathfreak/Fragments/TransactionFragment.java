package com.medipic.mathfreak.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Handler;

import com.medipic.mathfreak.Activities.GameScreen;
import com.medipic.mathfreak.Model.Question;
import com.medipic.mathfreak.R;

public class TransactionFragment extends Fragment {

    final long transactionFragmentWaitTime = 2000;

    Runnable runnable;
    Handler handler;
    GameScreen gameScreen;
    Question question;
    Bundle bundle;
    TextView transaction_text;

    ImageView imageView;
    View view;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.transaction_page,container,false);
        gameScreen = (GameScreen) getActivity();
        question = gameScreen.getQuestion();

        gameScreen.setQuestionStr(question.getQuestion(gameScreen.diff));

        bundle = this.getArguments();
        transaction_text = view.findViewById(R.id.transaction_text);

        setText();
        newQuestion();
        return view;
    }

    public void newQuestion(){
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                gameScreen.setFragment(2);
            }
        };


        handler.postDelayed(runnable, transactionFragmentWaitTime);
    }



    public void setText(){
            transaction_text.setText(String.valueOf(getResources().getString(R.string.question)+" "
                    +question.getQuesNum()));
    }


    public String getResultStr(){
        if(bundle.getBoolean("result"))
            return "CORRECT :)\n";
        else
            return "FALSE :/\n";
    }

    public Handler getHandler() {
        return handler;
    }

    public Runnable getRunnable() {
        return runnable;
    }
}
