package com.medipic.mathfreak.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.medipic.mathfreak.Utils.Measure;
import com.medipic.mathfreak.R;

public class SplashScreen extends AppCompatActivity implements Animation.AnimationListener{

    RelativeLayout logo_layout,text_layout;
    Animation upToDown,downToUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        Measure measure = new Measure(this);

        upToDown = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.splash_logo_anim);
        downToUp = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.splash_text_anim);

        logo_layout = findViewById(R.id.splash_logo_layout);
        text_layout = findViewById(R.id.splash_text_layout);

        logo_layout.startAnimation(upToDown);
        text_layout.startAnimation(downToUp);

        upToDown.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        startActivity(new Intent(this,MainScreen.class));
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
