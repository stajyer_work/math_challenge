package com.medipic.mathfreak.Activities;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.medipic.mathfreak.Fragments.QuestionFragment;
import com.medipic.mathfreak.Utils.ProgressBarLoader;
import com.medipic.mathfreak.Model.Level;
import com.medipic.mathfreak.Model.Score;

import com.medipic.mathfreak.Model.Question;
import com.medipic.mathfreak.Model.Timer;
import com.medipic.mathfreak.Fragments.TransactionFragment;
import com.medipic.mathfreak.R;
import com.medipic.mathfreak.Utils.GeneralTimer;
import com.medipic.mathfreak.Utils.LottieAnimations;
import com.medipic.mathfreak.Utils.ProgressBarPoints;

import java.util.ArrayList;
import java.util.Random;


public class GameScreen extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener{

    int [] btn_ids = {R.id.btn_1,R.id.btn_2,R.id.btn_3,R.id.btn_4,R.id.btn_5,R.id.btn_6,R.id.btn_7,R.id.btn_8,R.id.btn_9,R.id.btn_0};
    public static int diff;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView2;

    String questionStr;
    ArrayList<String> answerList;

    TextView top_bar_text,btn_minus;
    ImageView ok,delete;
    RelativeLayout game_screen_layout;
    ProgressBar count_progress;

    FragmentTransaction fragmentTransaction;
    Fragment fr;

    ProgressBarPoints progressBarPoints;
    GeneralTimer generalTimer;
    Handler progressLoader ;
    Question question;
    Bundle bundle;
    LottieAnimations lottieAnim;
    Score score;
    Level level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        setContentView(R.layout.game_screen);

        MobileAds.initialize(this,
                "ca-app-pub-3940256099942544/1033173712");

        mAdView2 = findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView2.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

        count_progress = findViewById(R.id.question_count_progress);
        game_screen_layout = findViewById(R.id.game_screen_layout);
        generalTimer = new GeneralTimer();
        question = new Question();
        level = new Level(getApplicationContext());
        progressBarPoints = new ProgressBarPoints(getApplicationContext(),count_progress);
        progressLoader =  new Handler();

        lottieAnim = new LottieAnimations(this,game_screen_layout);
        bundle = new Bundle();
        setTopBar();
        setButtons();
        try {
            setFragment(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        score = new Score(diff);

    }


    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {
        int action = motionEvent.getAction();

        if (action == MotionEvent.ACTION_DOWN) {
            v.animate().scaleXBy(100f).setDuration(5000).start();
            v.animate().scaleYBy(100f).setDuration(5000).start();
            return true;
        } else if (action == MotionEvent.ACTION_UP) {
            v.animate().cancel();
            v.animate().scaleX(1f).setDuration(1000).start();
            v.animate().scaleY(1f).setDuration(1000).start();
            return true;
        }

        return false;
    }

    public int getDiff() {
        return diff; }

    public ProgressBarPoints getProgressBarPoints(){
        return progressBarPoints;
    }

    public GeneralTimer getGeneralTimer() {
        return generalTimer;
    }

    public String getQuestionStr() {
        return questionStr;
    }

    public void setQuestionStr(String str){
        this.questionStr=str;
    }
    public Question getQuestion() {
        return this.question;
    }

    public Bundle getBundle() {
        return this.bundle;
    }

    public void createResultDialog(){
        // calculating general game time
        generalTimer.stopTimer();
        // getting all result variables
        int score = this.score.getScore();
        int levelCurrentXP = level.getLevelCurrentXP();
        int levelTotalXP = level.getLevelTotalXP();
        int levell = level.getLevel();
        float earnedXP = level.calculateXPfromScore(score,generalTimer.getTime(),this.diff);

        //save xps
        if(levelCurrentXP+earnedXP >= levelTotalXP){
            level.levelUP();
            level.saveCurrentXP(levelCurrentXP+Math.round(earnedXP)-levelTotalXP);
        } else {
            level.saveCurrentXP(levelCurrentXP+Math.round(earnedXP));
        }
        // dialog creating
        final Dialog customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        customDialog.setContentView(R.layout.score_alert_dialog_layout);

        //Progressbar and Progress Container view components are initialize
        ProgressBar progressBar = customDialog.findViewById(R.id.alert_progress);
        progressBar.setMax(levelTotalXP);
        progressBar.setProgress(levelCurrentXP);

        // score text
        TextView tv = customDialog.findViewById(R.id.score_text);
        tv.setText(String.valueOf(Math.round(earnedXP)));

        // Animated xp text
        TextView tv2 = customDialog.findViewById(R.id.xp_text);
        tv2.setText(String.valueOf(Math.round(earnedXP)));

        // Level text
        TextView level_text = customDialog.findViewById(R.id.lvl_text);
        level_text.setText(String.valueOf(getResources().getString(R.string.level)+levell));

        // Dialog play again button listener
        customDialog.findViewById(R.id.play_again_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random r = new Random();
                if (mInterstitialAd.isLoaded()) {
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    mInterstitialAd.show();
                                    mInterstitialAd.setAdListener(new AdListener() {
                                        @Override
                                        public void onAdClosed() {
                                            // Load the next interstitial.
                                            Intent intent =  new Intent(getApplicationContext(),GameScreen.class);
                                            intent.putExtra(MainScreen.DiffTAG,getDiffStr());
                                            startActivity(intent);
                                            customDialog.dismiss();
                                            finish();
                                        }

                                    });

                                }
                            },
                            r.nextInt(7000 - 5000));
                }
            }
        });



        // Dialog back_home button listener
        customDialog.findViewById(R.id.back_home_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random r = new Random();
                if (mInterstitialAd.isLoaded()) {
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    mInterstitialAd.show();
                                    mInterstitialAd.setAdListener(new AdListener() {
                                        @Override
                                        public void onAdClosed() {
                                            // Load the next interstitial.
                                            Intent intent = new Intent(getApplicationContext(),MainScreen.class);
                                            startActivity(intent);
                                            customDialog.dismiss();
                                            finish();
                                        }

                                    });

                                }
                            },
                            r.nextInt(7000 - 5000));
                }

            }
        });

        customDialog.show();

        try{
            // XP animation setting
            Animation anim = AnimationUtils.loadAnimation(this,R.anim.scale_and_translate_up);
            anim.setFillAfter(true);
            tv2.startAnimation(anim);

        }catch (Exception e){
            e.printStackTrace();
        }

        //saving scores


        // Progressbar loading animation class initialize and load animation
        ProgressBarLoader progressBarLoader = new ProgressBarLoader(this,progressBar, level_text,earnedXP,levelCurrentXP,levelTotalXP);




    }

    public String getDiffStr(){
        return MainScreen.DIFFICULTY_TAGS[diff-1];
    }

    public void setFragment(int i){

        if (!isFinish()) {
            FragmentManager fm = getFragmentManager();
            fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.animator.slide_left_enter, R.animator.slide_left_exit,
                    R.animator.slide_rigth_enter, R.animator.slide_right_exit);
            fr = null;

            switch (i) {
                case 1:
                    fr = new TransactionFragment();
                    fr.setArguments(bundle);
                    break;

                case 2:
                    answerList = new ArrayList<>();
                    fr = new QuestionFragment();
                    break;
            }

            setButtonsClickable();
            fragmentTransaction.replace(R.id.question_container, fr);
            fragmentTransaction.commit();
        }
    }


    private boolean isFinish(){
        if(question.getQuesNum() == (Question.QUESNUM + 1))
            return true;

        return false;
    }


    public void setButtonsClickable(){
        if(fr instanceof QuestionFragment){
            ok.setClickable(true);
            delete.setClickable(true);
        }else{
            ok.setClickable(false);
            delete.setClickable(false);
        }

    }

    public void setTopBar(){
        try{
            Bundle extras = getIntent().getExtras();
            View includedView = findViewById(R.id.top_bar_game);
            top_bar_text = includedView.findViewById(R.id.top_bar_text);
            if (extras != null) {
                String diffStr = extras.getString("Difficulty");
                setDiff(diffStr);
                top_bar_text.setText(diffStr);
            }
        }catch (NullPointerException e){
            Toast.makeText(this,"Hata : G001",Toast.LENGTH_LONG).show();
        }
    }

    public void setButtons(){

        btn_minus = findViewById(R.id.btn_minus);
        ok = findViewById(R.id.ok_btn);
        delete = findViewById(R.id.delete_btn);
        btn_minus.setText("-");

        Resources r = getResources();
        String name = getPackageName();

        for(int i=0 ;i<btn_ids.length;i++){
            View view = findViewById(btn_ids[i]);
            TextView tv = view.findViewById(R.id.btn_text);


            if(i==9){
                tv.setId(0);
                tv.setText(String.valueOf(0));
            }
            else{
                tv.setId(i+1);
                tv.setText(String.valueOf(i+1));
            }


        }
    }

    public void answerCorrect(){
        bundle.putBoolean("result",true);
        score.incCorrectN();
        try{
            lottieAnim.playCorrectAnim();

        }catch (Exception e){
            e.printStackTrace();
        }
        Log.e("Correct ",score.getCorrectN()+"");
        if(question.getQuesNum() == (Question.QUESNUM)) {
            createResultDialog();
            Timer.countDownTimer.cancel();
        } else {
            setFragment(1);
        }
    }

    public void answerFalse(){
        bundle.putBoolean("result",false);
        score.incFalseN();
        try{
            lottieAnim.playFalseAnim();

        }catch (Exception e){
            e.printStackTrace();
        }
        Log.e("False ",score.getFalseN()+"");
        if(question.getQuesNum() == (Question.QUESNUM)) {
            createResultDialog();
            Timer.countDownTimer.cancel();
        } else {
            setFragment(1);
        }    }

    public void ok(View view){

        try{
            if(question.getCorrectAnswer() == getAnswer()){
                answerCorrect();
            }
            else{
                answerFalse();
            }
        }catch(NumberFormatException e){
            e.printStackTrace();
        }

    }
    public void delete(View view){
        answerList.remove(answerList.size()-1);
        setTextView(answerList);
    }

    public int getAnswer(){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0;i<answerList.size();i++){
            stringBuilder.append(answerList.get(i));
        }

        String str =  stringBuilder.toString();

        return Integer.valueOf(stringBuilder.toString());
    }

    public void onClickButton(View view) {
        try{
            if(view.getId() == R.id.btn_minus)
                answerList.add("-");
            else
            answerList.add(String.valueOf(view.getId()));
            setTextView(answerList);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void toastScreen(int i){
        Toast.makeText(this, String.valueOf(i),Toast.LENGTH_SHORT).show();
    }

    public void setDiff(String diffStr){
        switch (diffStr){
            case "EASY":
                diff =1;
                break;
            case "MEDIUM":
                diff =2;
                break;
            case "HARD":
                diff =3;
                break;
        }
    }

    @Override
    public void onClick(View view) {

    }
    @Override
    public void onBackPressed(){
        Random r = new Random();
        if (mInterstitialAd.isLoaded()) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            mInterstitialAd.show();
                        }
                    },
                    r.nextInt(7000 - 5000));
        }
        if(fr instanceof TransactionFragment){
            ((TransactionFragment) fr).getHandler().removeCallbacks(((TransactionFragment) fr).getRunnable());
        }
        finish();
    }

    public void setTextView(ArrayList<String> arr ){
        StringBuilder stringBuilder = new StringBuilder();
        TextView tv = findViewById(R.id.question_text);

        stringBuilder.append(QuestionFragment.questionStr);
        stringBuilder.append(" = ");

        try{
            for(int i=0;i<arr.size();i++){
                stringBuilder.append(String.valueOf(arr.get(i)));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        tv.setText(stringBuilder.toString());
    }
}