package com.medipic.mathfreak.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.ads.AdView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.games.Player;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.medipic.mathfreak.Utils.ShareContent;
import com.medipic.mathfreak.Model.Level;
import com.medipic.mathfreak.R;
import com.medipic.mathfreak.Utils.Leaderboards;

import java.util.Objects;
import java.util.zip.Inflater;

public class MainScreen extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener{

    TextView top_bar_text,player_name_text;
    Button easy,medium,hard;
    RelativeLayout profile_card;
    ImageView pp;
    Context context;
    public GoogleSignInClient mGoogleSignInClient;
    public SharedPreferences preferences;

    private Level level;
    private LeaderboardsClient mLeaderboardsClient;
    public static int Diff = 0;
    private static final int RC_UNUSED = 5001;
    private static final int RC_SIGN_IN = 9001;
    public static String DiffTAG = "Difficulty";
    private final int TOTAL_LEADERBOARD_COUNT = 6;

    public final static String DIFFICULTY_TAGS [] = {"EASY","MEDIUM","HARD"};
    private final String signInKey = "SIGNED";

    public static String TAG = "MainScreen.java";
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.main_screen);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        preferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);

        View includedLayout = findViewById(R.id.top_bar_main);
        top_bar_text = includedLayout.findViewById(R.id.top_bar_text);
        top_bar_text.setText(getResources().getString(R.string.app_name));
        easy = findViewById(R.id.easy);
        medium = findViewById(R.id.medium);
        hard = findViewById(R.id.hard);

        easy.setOnClickListener(this);
        medium.setOnClickListener(this);
        hard.setOnClickListener(this);
        profile_card =findViewById(R.id.profile_card);
        pp = findViewById(R.id.profile_card_pp);
        player_name_text=findViewById(R.id.player_name);

        setProfile_card();
        MobileAds.initialize(this,
                "ca-app-pub-3940256099942544/1033173712");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        signInSilently();
    }

    public void setProfile_card(){
        Level level = new Level(this);
        ProgressBar profile_card_progress = findViewById(R.id.profile_card_progress);
        profile_card_progress.setMax(level.getLevelTotalXP());
        profile_card_progress.setProgress(level.getLevelCurrentXP());
        TextView tv = findViewById(R.id.profile_card_level_text);
        tv.setText(String.valueOf(getResources().getString(R.string.level)+level.getLevel()));
        setHighScores();
    }

    public void setHighScores(){
        TextView tv = findViewById(R.id.easy_high_score_text);
        tv.setText(getHighScore(R.string.easy_high_score_tag));
        tv = findViewById(R.id.medium_high_score_text);
        tv.setText(getHighScore(R.string.medium_high_score_tag));
        tv = findViewById(R.id.hard_high_score_text);
        tv.setText(getHighScore(R.string.hard_high_score_tag));
    }

    private String getHighScore(int id){
        return String.valueOf(preferences.getInt(getResources().getString(id),0));
    }

    public void signIn(View view){
        if(!isSignedIn())
        startSignInIntent();
        else{
            Toast.makeText(this,"You already have signed in ! ",Toast.LENGTH_LONG).show();
        }
    }

    public void helpScreen(View view){

        /* TODO Eski kod*/
       /* final Dialog helpDialog = new Dialog(this);
        helpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        helpDialog.requestWindowFeature(Window.FEATURE_SWIPE_TO_DISMISS);
        helpDialog.setCancelable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(helpDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        }

        helpDialog.setContentView(R.layout.help_screen);
        helpDialog.show();*/

        LayoutInflater layoutInflater = (LayoutInflater) MainScreen.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.help_screen,null);
        float density=MainScreen.this.getResources().getDisplayMetrics().density;
        final PopupWindow popupWindow = new PopupWindow(layout,(int)density*360, (int)density*320, true);
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);

        layout.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

    }

    public void leaderboard(View view){


        if(!isSignedIn()){
            Dialog warnDialog = new Dialog(this);
            warnDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            warnDialog.setCancelable(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(warnDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
            }
            warnDialog.setContentView(R.layout.leaderboard_warning);
            warnDialog.show();
        } else {
            // dialog initialize
            Dialog customDialog = new Dialog(this);
            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            customDialog.setCancelable(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
            }
            customDialog.setContentView(R.layout.leaderboards_alert_dialog);

            //Creating level object for getting level
            level = new Level(getApplicationContext());
            int levelN = level.getLevel();
            String strLevel = getStr(R.string.level_str);
            String strGeneral = getStr(R.string.general);
            //Buttons initialize
            TextView tv = customDialog.findViewById(R.id.alert_tv1);
            tv.setText(String.valueOf(DIFFICULTY_TAGS[0] + "\n" + strGeneral));
            tv = customDialog.findViewById(R.id.alert_tv2);
            tv.setText(String.valueOf(DIFFICULTY_TAGS[0] + "\n" + strLevel + " " + levelN));
            tv = customDialog.findViewById(R.id.alert_tv3);
            tv.setText(String.valueOf(DIFFICULTY_TAGS[1] + "\n" + strGeneral));
            tv = customDialog.findViewById(R.id.alert_tv4);
            tv.setText(String.valueOf(DIFFICULTY_TAGS[1] + "\n" + strLevel + " " + levelN));
            tv = customDialog.findViewById(R.id.alert_tv5);
            tv.setText(String.valueOf(DIFFICULTY_TAGS[2] + "\n" + strGeneral));
            tv = customDialog.findViewById(R.id.alert_tv6);
            tv.setText(String.valueOf(DIFFICULTY_TAGS[2] + "\n" + strLevel + " " + levelN));

            customDialog.findViewById(R.id.easy_general).setOnClickListener(this);
            customDialog.findViewById(R.id.easy_level).setOnClickListener(this);
            customDialog.findViewById(R.id.medium_general).setOnClickListener(this);
            customDialog.findViewById(R.id.medium_level).setOnClickListener(this);
            customDialog.findViewById(R.id.hard_general).setOnClickListener(this);
            customDialog.findViewById(R.id.hard_level).setOnClickListener(this);

            customDialog.show();
        }

        /*
        Dialog soonDialog = new Dialog(this);
        soonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        soonDialog.setCancelable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(soonDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        }
        soonDialog.setContentView(R.layout.coming_soon);
        soonDialog.show();
        */
    }

    private String getStr(int id){
        return " "+getResources().getString(id);
    }
    public void share(View view){
        /* TODO İlker'in ShareContent class'ı. Profil resmi ile birlikte göndermek için kullanılabilir. Şu anda çalışmıyor */

        /* ShareContent sharecontent = new ShareContent(getApplicationContext());
        sharecontent.share(profile_card);
        */

        //Metin paylaşma
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Download Math Challenge and challenge your friends!");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    /* TODO Boş hesap metodu
    public void account(View view){

    }
    */

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.easy:
                Diff = 1;
                startGame(DIFFICULTY_TAGS[0].toUpperCase());
                break;
            case R.id.medium:
                Diff = 2;
                startGame(DIFFICULTY_TAGS[1].toUpperCase());
                break;
            case R.id.hard:
                Diff = 3;
                startGame(DIFFICULTY_TAGS[2].toUpperCase());
                break;
            case R.id.easy_general:
                showLeaderBoard(new Leaderboards().getEasy_ids()[0]);
                break;
            case R.id.easy_level:
                showLeaderBoard(new Leaderboards().getEasy_ids()[level.getLevel()]);
                break;
            case R.id.medium_general:
                showLeaderBoard(new Leaderboards().getMedium_ids()[0]);
                break;
            case R.id.medium_level:
                showLeaderBoard(new Leaderboards().getMedium_ids()[level.getLevel()]);
                break;
            case R.id.hard_general:
                showLeaderBoard(new Leaderboards().getHard_ids()[0]);
                break;
            case R.id.hard_level:
                showLeaderBoard(new Leaderboards().getHard_ids()[level.getLevel()]);
                break;
        }
    }

    public void startGame(String difficulty){
        Intent intent = new Intent(this,GameScreen.class);
        intent.putExtra(DiffTAG,difficulty);
        startActivity(intent);
    }

    private boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(this) != null;
    }

    private void startSignInIntent() {
        startActivityForResult(mGoogleSignInClient.getSignInIntent(), RC_SIGN_IN);
    }

    private void signInSilently() {
        Log.d(TAG, "signInSilently()");
        mGoogleSignInClient = GoogleSignIn.getClient(this,
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN).build());
        mGoogleSignInClient.silentSignIn().addOnCompleteListener(this,
                new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInSilently(): success");
                            GoogleSignInAccount signedInAccount = task.getResult();
                            onConnected(task.getResult());
                        } else {
                            Log.d(TAG, "signInSilently(): failure", task.getException());
                            onDisconnected();
                        }


                    }
                });
    }

    // TODO Çıkış yapma metodu
    private void signOut() {
        Log.d(TAG, "signOut()");

        if (!isSignedIn()) {
            Log.w(TAG, "signOut() called, but was not signed in!");
            return;
        }

        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        boolean successful = task.isSuccessful();
                        Log.d(TAG, "signOut(): " + (successful ? "success" : "failed"));

                        onDisconnected();
                    }
                });
    }

    public void showLeaderBoard(int id) {
        mLeaderboardsClient.getLeaderboardIntent(getResources().getString(id))
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_UNUSED);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        handleException(e, "LeaderBoards Exception ! ");
                    }
                });
    }

    private void handleException(Exception e, String details) {
        int status = 0;

        if (e instanceof ApiException) {
            ApiException apiException = (ApiException) e;
            status = apiException.getStatusCode();
        }

        @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String message = getString(R.string.status_exception_error, details, status, e);

        new AlertDialog.Builder(MainScreen.this)
                .setMessage(message)
                .setNeutralButton(android.R.string.ok, null)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);

            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                onConnected(account);
            } catch (ApiException apiException) {
                String message = String.valueOf(apiException.getStatusCode());
                //String message = apiException.getMessage();
                if (message == null || message.isEmpty()) {
                    message = "ERROR CODE : 2318";
                }

                onDisconnected();

                new AlertDialog.Builder(this)
                        .setMessage(message)
                        .setNeutralButton(android.R.string.ok, null)
                        .show();
            }
        }
    }

    private void onDisconnected() {
        Log.d(TAG, "onDisconnected()");
        mLeaderboardsClient = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void loadPP(GoogleSignInAccount googleSignInAccount){
        final ImageManager loader = ImageManager.create(this);
        Games.getPlayersClient(this,googleSignInAccount)
                .getCurrentPlayer()
                .addOnSuccessListener(
                        new OnSuccessListener<Player>() {
                            @Override
                            public void onSuccess(Player player) {
                                try{
                                    Uri pImageUri = player.getIconImageUri();
                                    loader.loadImage(pp,pImageUri);
                                    player_name_text.setText(player.getName());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                )
                .addOnFailureListener(createFailureListener("There was a problem getting the player!"));
    }

    private void onConnected(GoogleSignInAccount googleSignInAccount) {
        Log.d(TAG, "onConnected(): connected to Google APIs");
        mLeaderboardsClient = Games.getLeaderboardsClient(this, googleSignInAccount);
        preferences.edit().putBoolean(signInKey,true).apply();

        //Loading profile picture of Player after signed
        loadPP(googleSignInAccount);

        Log.e("LeaderBoardsClient ","Created !");
    }

    private OnFailureListener createFailureListener(final String string) {
        return new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                handleException(e, string);
            }
        };
    }

   @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        signInSilently();
        // Since the state of the signed in user can change when the activity is not active
        // it is recommended to try and sign in silently from when the app resumes.
    }

    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            v.animate().scaleXBy(100f).setDuration(5000).start();
            v.animate().scaleYBy(100f).setDuration(5000).start();
            return true;
        } else if (action == MotionEvent.ACTION_UP) {
            v.animate().cancel();
            v.animate().scaleX(1f).setDuration(1000).start();
            v.animate().scaleY(1f).setDuration(1000).start();
            return true;
        }

        return false;
    }

    private class SignSilentBackground extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            if(preferences.getBoolean(signInKey,false)){
                signInSilently();
            }
            return null;
        }
    }

}
